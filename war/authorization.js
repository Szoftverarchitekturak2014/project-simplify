/**
 * Javascript for authorization and filemanagement
 */
var CLIENT_ID = '920432111499-dnj11cqjt7vcn6sbadi142pn6vcghheh.apps.googleusercontent.com';
var SCOPES = [ 'https://www.googleapis.com/auth/drive',
		'https://www.googleapis.com/auth/drive.install',
		'https://www.googleapis.com/auth/drive.file',
		'https://www.googleapis.com/auth/userinfo.profile' ];
var SCRIPT_URL = 'https://script.google.com/macros/s/AKfycbz_OU1qEwcZ7B6yhQB0F0S8eLRNpvdnAf0GtztwgXLs/dev?url=';

/**
 * Called when the client library is loaded to start the auth flow.
 */
function handleClientLoad() {
	window.setTimeout(checkAuth, 1000);
}

/**
 * Check if the current user has authorized the application.
 */
function checkAuth() {
	gapi.auth.authorize({
		'client_id' : CLIENT_ID,
		'scope' : SCOPES.join(' '),
		'immediate' : true
	}, handleAuthResult);
}

function getParameter(name) {
	if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)'))
			.exec(location.search))
		return decodeURIComponent(name[1]);
}

/**
 * Called when authorization server replies.
 *
 * @param {Object} authResult Authorization result.
 */
function handleAuthResult(authResult) {
	if (authResult && !authResult.error) {
		// Access token has been successfully retrieved, requests can be sent to the API.
		var param = getParameter('state');
		var jsonparam = JSON.parse(param);
		var fileId = jsonparam.exportIds;
		var action = jsonparam.action;

		if (action == "open") {
			gapi.client.load('drive', 'v2', function() {
				openFile(fileId,action);
			});
		} else if (action == "create") {
			gapi.client.load('drive', 'v2', function() {
				var request = gapi.client.request({
					'path' : '/drive/v2/files',
					'method' : 'POST',
					'body' : {
						"title" : "My Project.sprj",
						"mimeType" : "application/vnd.google-apps.spreadsheet",
						"description" : "New Project"
					}
				});
				request.execute(function(resp) {
					console.log(resp);
					openFile(resp.id,action);

				});
			});
		} else {
			console.log("Unsupported operation: " + action);
		}
	} else {
		// No access token could be retrieved, authorizing
		gapi.auth.authorize({
			'client_id' : CLIENT_ID,
			'scope' : SCOPES,
			'immediate' : false
		}, handleAuthResult);
	}

}

/**
 * Download a file's content.
 *
 * @param {FileId} file Drive FileID.
 * @param {Function} callback Function to call when the request is complete.
 */
function openFile(fileId,action) {
	var file = gapi.client.drive.files.get({
		'fileId' : fileId
	});
	var fileUrl;
	file.execute(function(resp) {
		fileUrl = resp.alternateLink;
		if (fileUrl) {
			if(action == "open"){
				window.open(fileUrl, "_self");
			}
			if(action == "create"){
				createGUI(fileUrl);
			}
		} else {
			return;
		}
	});
}

function createGUI(fileUrl) {
	window.location.href = SCRIPT_URL+fileUrl;
	console.log(SCRIPT_URL+fileUrl);
}